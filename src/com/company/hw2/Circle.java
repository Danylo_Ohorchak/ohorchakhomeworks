package com.company.hw2;

public class Circle {
    private double radius;
    private double diametr = radius * 2;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getDiametr() {
        return diametr;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {
    }

    public double calculateSquare() {
        double r = this.diametr / 2;
        double square = Math.PI * r * r;
        return square;
    }

    public double calculateCircleLength() {
        double circleLength = 2 * Math.PI * this.radius;
        return circleLength;
    }
}
