package com.company.hw2;

public class Rectangle {
    private double length;
    private double width;

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    public Rectangle() {
        this.length = 10;
        this.width = 10;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double calculateSquare() {
        double square = length * width;
        return square;
    }

    public double calculatePerimetr() {
        double perimetr = 2 * (length + width);
        return perimetr;
    }
}
