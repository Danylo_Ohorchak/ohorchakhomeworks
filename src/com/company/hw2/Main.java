package com.company.hw2;

public class Main {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(11, 16);
        System.out.println("Для 1 прямокутника:");
        System.out.println("Периметр прямокутника = " + rectangle1.calculatePerimetr() + "\n"
                + "Площа прямокутника = " + rectangle1.calculateSquare());
        System.out.println("Для 2 прямокутника:");
        System.out.println("Периметр прямокутника = " + rectangle2.calculatePerimetr() + "\n"
                + "Площа прямокутника = " + rectangle2.calculateSquare());
        Circle circle = new Circle(10);
        System.out.println("Площа круга = " + circle.calculateSquare() + "\n" + "Довжина кола = "
                + circle.calculateCircleLength());
    }
}
