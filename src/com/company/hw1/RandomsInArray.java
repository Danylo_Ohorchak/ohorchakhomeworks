package com.company.hw1;

import java.util.Random;
import java.util.Scanner;

public class RandomsInArray {

    public static void main(String[] args) {
        Scanner scanner =  new Scanner(System.in);
        RandomsInArray main = new RandomsInArray();
        System.out.print("Введіть розмірність масиву: ");
        int n = scanner.nextInt();
        int[] randomArr = new int[n];
        main.putRandoms(randomArr);
        main.printArray(randomArr);
        System.out.println("Мінімальний елемент = " + main.minElement(randomArr)+ "   Максимальний елемент = " + main.maxElement(randomArr));

    }

    public void putRandoms(int[] arr){
        Random random = new Random();
        for(int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(100);
        }
    }

    public int maxElement(int[] arr){
        int max = arr[0];
        for(int i = 0; i < arr.length; i++) {
            if(arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }

    public int minElement(int[] arr){
        int min = arr[0];
        for(int i = 0; i < arr.length; i++) {
            if(arr[i] < min) {
                min = arr[i];
            }
        }
        return min;
    }

    public void printArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
