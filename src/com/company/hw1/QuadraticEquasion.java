package com.company.hw1;

import java.util.Scanner;

public class QuadraticEquasion {
    public static void main(String[] args) {
        QuadraticEquasion main = new QuadraticEquasion();
        Scanner scanner = new Scanner(System.in);
        int i = 0;
        while(i == 0) {
            System.out.print("Введіть a,b i c: ");
            double a = scanner.nextDouble();
            double b = scanner.nextDouble();
            double c = scanner.nextDouble();
            double d = main.discriminant(a,b,c);
            if(d < 0) {
                System.out.println("Немає розвязків!");
            }
            else if(d == 0) {
                double x = main.firstRoot(a,b,d);
                System.out.println("Рівняннямає один розвязок x = " + x);
            }
            else {
                double x1 = main.firstRoot(a,b,d);
                double x2 = main.secondRoot(a,b,d);
                System.out.println("Рівняння має два розвязки х1 = " + x1 +"  х2 = " + x2);
            }

            System.out.print("Введіть і рівне 0 щоб розвязати ще одне рівняння, або віддмінне від 0 щоб припинити:");
            i = scanner.nextInt();
        }

    }

    public double discriminant(double a, double b, double c) {
        double d = b*b - 4*a*c;
        return d;
    }

    public double firstRoot(double a, double b, double d) {
        double x = (-b + Math.sqrt(d))/2*a;
        return x;
    }

    public double secondRoot(double a, double b, double d) {
        double x = (-b - Math.sqrt(d))/2*a;
        return x;
    }
}
