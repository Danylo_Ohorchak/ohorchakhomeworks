package com.company.hw1;

import java.util.Scanner;

public class CloserToTen {
    public static void main(String[] args) {
        CloserToTen main =  new CloserToTen();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введіть m та n: ");
        double n = scanner.nextDouble();
        double m = scanner.nextDouble();
        main.closerTo(n,m,10);
    }

    public void closerTo(double n,double m,double a) {
        double firstDistance = Math.abs(n - a);
        double secondDistance = Math.abs(m - a);
        if(firstDistance > secondDistance ) {
            System.out.println(m + " ближче до " + a + " ніж " + n);
        }

        else if(firstDistance == secondDistance) {
            System.out.println(m + " i " + n + " рівновіддалені від " + a);
        }

        else {
            System.out.println(n + " ближче до " + a + " ніж " + m);
        }

    }

}
