package com.company.hw1;

import java.util.Scanner;

public class SwitchTask {

    public static void main(String[] args) {

        SwitchTask main = new SwitchTask();
        Scanner scanner = new Scanner(System.in);
        String[] monday;
        String[] tuesday;
        String[] wednesday;
        String[] thursday;
        String[] friday;
        String[] saturday;
        String[] sunday;
        int i = 0;
        System.out.print("Введіть кількість завдань на день: ");
        int n = scanner.nextInt();
        monday = new String[n];
        tuesday = new String[n];
        wednesday = new String[n];
        thursday = new String[n];
        friday = new String[n];
        saturday = new String[n];
        sunday = new String[n];
        main.tasksInput(monday, scanner, "понеділок");
        main.tasksInput(tuesday, scanner, "вівторок");
        main.tasksInput(wednesday, scanner, "середу");
        main.tasksInput(thursday, scanner, "четвер");
        main.tasksInput(friday, scanner, "п'ятницю");
        main.tasksInput(saturday, scanner, "суботу");
        main.tasksInput(sunday, scanner, "неділю");

        while (i == 0) {

            System.out.println("На який день показати розклад?");
            String day = scanner.next();

            switch (day) {
                case "Понеділок":
                    main.tasksOutput(monday,"понеділок" );
                    break;
                case "Вівторок":
                    main.tasksOutput(tuesday,"вівторок" );
                    break;
                case "Середа":
                    main.tasksOutput(tuesday,"середу");
                    break;
                case "Четвер":
                    main.tasksOutput(tuesday, "четвер");
                    break;
                case "Пятниця":
                    main.tasksOutput(tuesday, "п'ятницю");
                    break;
                case "Субота":
                    main.tasksOutput(tuesday, "суботу");
                    break;
                case "Неділя":
                    main.tasksOutput(tuesday,"неділю");
                    break;
                default:
                    System.out.println("Неправильно введений день, попробуйте ще раз!");
            }

            System.out.print("Введіть і = 0 щоб подивитися розклад інший день, або і відмінне від 0 щоб продовжити:");
            i = scanner.nextInt();
        }
    }


    public void tasksInput(String[] a, Scanner scanner, String b) {
        System.out.println("Введіть завдання на " + b + ":");
        for (int i = 0; i < a.length; i++) {
            a[i] = scanner.nextLine();
        }
    }

    public void tasksOutput(String[] a, String b) { //в b передаєм день тижня.
        if (a.length == 0) {
            System.out.println("Немає завдань на " + b);
        }

        else {
            System.out.println("Завдання на " + b);
            for (int i = 0; i < a.length; i++) {
                System.out.println((i+1) + ": " + a[i]);
            }

        }

    }
}
