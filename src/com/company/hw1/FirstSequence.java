package com.company.hw1;

public class FirstSequence {
    public static void main(String[] args) {
        FirstSequence main = new FirstSequence();
        int oddNumbers[] = new int[55];
        for(int i = 0;i < 55; i++) {
            oddNumbers[i] = 2 * i + 1;
        }
        main.printArray(oddNumbers);
    }

    public void printArray( int[] arr) {
        for(int i = 0;i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
