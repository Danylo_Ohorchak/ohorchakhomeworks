package com.company.hw1;

public class SecondSequence {

    public static void main(String[] args) {
        SecondSequence main = new SecondSequence();
        int sequenceArray[] = new int[19];
        int len = sequenceArray.length;
        for (int i = 18; i >= 0; i--) {
            sequenceArray[len - i -1] = i * 5;

        }

        main.printArray(sequenceArray);
    }

    public void printArray(int[] array){
        for(int i = 0; i< array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
