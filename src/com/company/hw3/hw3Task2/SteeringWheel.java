package com.company.hw3.hw3Task2;

public class SteeringWheel {
    private int steeringWheelDiametr;
    String SteeringWheelPosition;

    public String getSteeringWheelPosition() {
        return SteeringWheelPosition;
    }

    public void setSteeringWheelPosition(String steeringWheelPosition) {
        SteeringWheelPosition = steeringWheelPosition;
    }

    public int getWheelDiametr() {
        return steeringWheelDiametr;
    }

    public void setWheelDiametr(int wheelDiametr) {
        this.steeringWheelDiametr = wheelDiametr;
    }

    public SteeringWheel(int wheelDiametr, String steeringWheelPosition) {
        this.steeringWheelDiametr = wheelDiametr;
        this.SteeringWheelPosition = steeringWheelPosition;
    }

    public SteeringWheel() {
        this.steeringWheelDiametr = 0;
        this.SteeringWheelPosition = "None";
    }

    @Override
    public String toString() {
        return "SteeringWheel: " +
                "steeringWheelDiametr = " + steeringWheelDiametr +
                "  SteeringWheelPosition = '" + SteeringWheelPosition + '\'';
    }

    public void increaseStreeringWheelSizeBy(int sizeIncrease) {
        this.steeringWheelDiametr += sizeIncrease;
    }

    public void decreaseStreeringWheelSizeBy(int sizeDecrease) {
        this.steeringWheelDiametr -= sizeDecrease;
    }


}
