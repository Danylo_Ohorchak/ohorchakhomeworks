package com.company.hw3.hw3Task2;

public class Wheel {
    private int wheelSize;
    private String wheelSeason;

    public int getWheelSize() {
        return wheelSize;
    }

    public void setWheelSize(int wheelSize) {
        this.wheelSize = wheelSize;
    }

    public String getWheelSeason() {
        return wheelSeason;
    }

    public void setWheelSeason(String wheelSeason) {
        this.wheelSeason = wheelSeason;
    }

    public Wheel(int wheelSize, String wheelSeason) {
        this.wheelSize = wheelSize;
        this.wheelSeason = wheelSeason;
    }

    public Wheel() {
        this.wheelSize = 0;
        this.wheelSeason = "None";
    }

    @Override
    public String toString() {
        return "Wheel: " +
                "wheelSize = " + wheelSize +
                ", wheelSeason='" + wheelSeason + '\'';
    }

    public String changeWheel() {
        if (this.wheelSeason.equalsIgnoreCase("Winter")) {
            System.out.println("Changing wheels for summer");
            this.wheelSeason = "Summer";
        } else if (this.wheelSeason.equalsIgnoreCase("Summer")) {
            System.out.println("Changing wheels for winter");
            this.wheelSeason = "Winter";
        } else {
            System.out.println("You have no wheels, setting summer wheels as default");
            this.wheelSeason = "Summer";
        }
        return this.wheelSeason;

    }
}
