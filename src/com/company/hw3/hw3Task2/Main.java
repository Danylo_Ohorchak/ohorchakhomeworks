package com.company.hw3.hw3Task2;

public class Main {
    public static void main(String[] args) {
        Wheel wheel = new Wheel(21, "Summer");
        SteeringWheel steeringWheel = new SteeringWheel(30, "left");
        Body body = new Body(4.7, "Black");
        Car car = new Car("BMW", 3.0, 21, "Summer",
                30, "Left", "Black", 4.7);

        System.out.println(car.toString());

        car.makeCarSportier(4.0, 23);
        car.multiColorCar("Red");
        car.wheelChange();
        car.increaseSteeringWheel(5);

        System.out.println(car.toString());
    }
}
