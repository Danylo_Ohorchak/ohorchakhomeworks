package com.company.hw3.hw3Task2;

public class Car {
    private String brand;
    private double engineVolume;
    private Wheel wheel;
    private SteeringWheel steeringWheel;
    private Body body;

    public Car(String brand, double engineVolume, int wheelSize, String wheelSeason
            , int steeringWheelDiametr, String steeringWheelPosition, String color, double length) {
        this.brand = brand;
        this.engineVolume = engineVolume;
        this.wheel = new Wheel(wheelSize, wheelSeason);
        this.steeringWheel = new SteeringWheel(steeringWheelDiametr, steeringWheelPosition);
        this.body = new Body(length, color);
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getEngineVolume() {
        return engineVolume;
    }

    public void setEngineVolume(float engineVolume) {
        this.engineVolume = engineVolume;
    }

    public Wheel getWheel() {
        return wheel;
    }

    public void setWheel(Wheel wheel) {
        this.wheel = wheel;
    }

    public SteeringWheel getSteeringWheel() {
        return steeringWheel;
    }

    public void setSteeringWheel(SteeringWheel steeringWheel) {
        this.steeringWheel = steeringWheel;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Car:" +
                brand +
                "  Engine volume = " + engineVolume + '\n' +
                wheel + '\n' +
                steeringWheel + '\n' +
                body;
    }

    public void makeCarSportier(double engineVolume, int wheelSize) {
        this.engineVolume = engineVolume;
        this.wheel.setWheelSize(wheelSize);
    }

    public void wheelChange() {
        this.wheel.changeWheel();
    }

    public void increaseSteeringWheel(int increaseValue) {
        this.steeringWheel.increaseStreeringWheelSizeBy(increaseValue);
    }

    public void decreaseSteeringWheel(int decreaseValue) {
        this.steeringWheel.increaseStreeringWheelSizeBy(decreaseValue);
    }

    public void multiColorCar(String additionalColor) {
        this.body.addColor(additionalColor);
    }
}
