package com.company.hw3.hw3Task2;

public class Body {
    private double length;
    private String color;

    public Body() {
        this.length = 0;
        this.color = "None";
    }

    public Body(double length, String color) {
        this.length = length;
        this.color = color;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Body: " +
                "length=" + length +
                ", color = " + color + '\'';
    }

    public void addColor(String color) {
        this.color += " and " + color;
    }
}
